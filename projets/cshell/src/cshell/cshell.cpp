#include <cshell/cshell.hpp>
#include<iostream>
#include <fstream>
#include <string>

        ///\brief Implementation de la fonction pour la  Concaténation de fichiers
        ///
        void Cshell::cat(std::ostream & os,const std::vector<std::string>& args){
            ///\brief Parcours de liste fichier pour afficher
            ///
            for(int i=0;i<args.size();i++){
                std::ifstream fluxFichier(args[i]);
                if(fluxFichier){
                    ///\brief Lire tout le fichier
                    ///
                    std::string line;
                    while(std::getline(fluxFichier,line)){
                        os<<line<<std::endl;
                    }

                }else{
                    os<<"Errerur:Impossible de lire le fichier"<<std::endl;
                }
            }
            
        }
         ///\brief Implementatation de la fonction pour la Recherche dans les fichiers
        ///
        void Cshell::grep(std::ostream & os, const std::string & pattern, const std::vector<std::string>& args){
            ///\brief Parcours de liste fichier pour afficher
            ///
            bool trouve=false;
            for(int i=0;i<args.size();i++){
                std::ifstream fluxFichier(args[i]);
                if(fluxFichier){
                    ///\brief Lire tout le fichier
                    ///
                    std::string line;
                    while(std::getline(fluxFichier,line)){
                        if(line==pattern){
                            os<<line<<std::endl;
                            trouve=true;
                            break;
                        }
                    }
                    if(trouve) break;

                }else{
                    os<<"Errerur:Impossible de lire le fichier"<<std::endl;
                }
                if(!trouve) os<<"oups votre phrase n'existe pas";
            }
        }
         ///\brief Implementation de la fonction pour lesStatisques de fichiers
        ///
        void Cshell::wc(std::ostream & os, const std::vector<std::string>& args){
            ///\brief Parcours de liste fichier pour afficher
            ///
            int cptLine=0;
            int cptCar=0;
            for(int i=0;i<args.size();i++){
                std::ifstream fluxFichier(args[i]);
                if(fluxFichier){
                    ///\brief Lire tout le fichier
                    ///
                    std::string line;
                    while(std::getline(fluxFichier,line)){
                       cptLine+=1;
                       cptCar+=line.size();
                    }
                    

                }else{
                    os<<"Errerur:Impossible de lire le fichier"<<std::endl;
                }
            }
            os<<"Nombre de lignes : "<<cptLine<<std::endl;
            os<<"Nombre de caracteres : "<<cptCar<<std::endl;
        }

