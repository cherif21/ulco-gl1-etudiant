#include <iostream>
#include <exception>
#include <vector>

enum class NumError { TooLow, TooHigh, Is37, IsNegative };

// Reads a number in stdin. Throws a NumError if 37 or negative.
int readPositiveButNot37() {
    std::string line;
    std::cout << "enter num: ";
    std::flush(std::cout);
    std::getline(std::cin, line);
    int num = stod(line);
    // TODO handle errors
     if(num==37) throw NumError::Is37;
    if(num<0) throw NumError::IsNegative;
    return num;
}

// Reads a number in stdin. Throws a NumError if too low or too high.
std::vector<int> replicate42(int n) {
    // TODO handle errors
    return std::vector<int>(n, 42);
}

int main() {

    // TODO handle errors
    try{
        // read number
        const int num = readPositiveButNot37();
        std::cout << "num = " << num << std::endl;

        // build vector
        const auto v = replicate42(num);

        // print vector
        for (const int x : v)
            std::cout << x << " ";
        std::cout << std::endl;
    }
    catch(NumError e){
        switch (e)
        {
            case  NumError::Is37:
                    std::cout<<"error: 37"<<std::endl;
                break;
            case  NumError::IsNegative:
                    std::cout<<"error: Negative"<<std::endl;
                break;
            default:
                break;
        }
       
         return -1;
    }
    catch(const std::exception &e){
        std::cout<<"System Error "<<e.what()<<std::endl;
        return -1;
    }
    catch(...){
        std::cout<<"Error Inconnue"<<std::endl;
        return -1;
    }
    return 0;
}

