#include <mymaths/mymaths.hpp>

#include <catch2/catch.hpp>

TEST_CASE("add2 de 40"){
    REQUIRE(add2(40)==2);
}

TEST_CASE("add2 de 0"){
    REQUIRE(add2(0)==2);
}
