#include <gess/gess.hpp>
#include <random>

///\brief Implementation de la fonction newGame avec un nombre donnée
///
 void Game::newGame(int target){
     _target=target;
     _nbTries=5;
     _found=false;
 }
///\brief Implementation de la fonction newGame  sans parametres
///
void Game::newGame(){
    std::mt19937 e(std::random_device{}());
    std::uniform_int_distribution<int> dist(1,100);
    _target=dist(e);
     _nbTries=5;
     _found=false;
 }
///\brief Implementation de la fonction getTarget
///
int  Game::getTarget() const{
    return _target;
}
///\brief Implementation de la fonction play avec un entier pour jouer un coupt
///
Result Game::play(int i){
    //TODO gestion d'erreur
    //TODO tester si le status est en Running
    _nbTries--;
    if(i==_target){
        _found=true;
        return Result::Found;
    }
    if(i< _target) return Result::TooLow;
    if (i> _target) return Result::TooHigh;
    //TODO gestion erreur
}

Status Game::status()const {
    if(_found) return Status::Win;
    if(_nbTries<=0) return Status::Lost;
    return Status::Running;
}

void RunGame(Game & game, std::ostream & os, std::istream & is){
     while(game.status()==Status::Running){
       std::cout<<"number?"<<std::endl;
       int i;
       std::cin>>i;
       Result r=game.play(i);
       switch (r)
       {
       case Result::TooHigh:
            std::cout<<"Too High"<<std::endl;
           break;
        case Result::TooLow:
            std::cout<<"Too Low"<<std::endl;
           break;
        case Result::Found:
            std::cout<<"Found"<<std::endl;
            return;
        case Result::Invalid:
            std::cout<<"Invalid"<<std::endl;
           break;
       }
   }
}

