#include <cshell/cshell.hpp>
#include <fstream>
#include <iostream>

int main() {
    std::vector<std::string>tabFile;
    tabFile.push_back("data/testFichier.txt");
    Cshell sh;
    sh.cat(std::cout,tabFile);
    sh.grep(std::cout,"I am C++",tabFile);
    sh.wc(std::cout,tabFile);
    return 0;
}

