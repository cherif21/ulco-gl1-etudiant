#pragma once
#include<iostream>
#include<vector>

///\brief Class Cshell 
///
class Cshell{
    public:
        ///\brief Concaténation de fichiers
        ///
        void cat(std::ostream & os,const std::vector<std::string>& args);
         ///\brief Recherche dans les fichiers
        ///
        void grep(std::ostream & os, const std::string & pattern, const std::vector<std::string>& args);
         ///\brief Statisques de fichiers
        ///
        void wc(std::ostream & os, const std::vector<std::string>& args);
};

