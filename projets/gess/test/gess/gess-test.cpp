#include <gess/gess.hpp>

#include <catch2/catch.hpp>
///\brief Implementation de la fonction getTarget
///
TEST_CASE( "new Game 1" ) {
    Game game;
    game.newGame(42);
    int result=game.getTarget();
    int expected=42;
    REQUIRE(result==expected);
}
TEST_CASE( "play 1" ) {
    Game game;
    game.newGame(42);
    Result result=game.play(12);
    REQUIRE(result==Result::TooLow);
}
TEST_CASE( "play 2" ) {
    Game game;
    game.newGame(42);
   Result result=game.play(82);
    REQUIRE(result==Result::TooHigh);
}
TEST_CASE( "status 1" ) {
    Game game;
    game.newGame(42);
    Status result=game.status();
    REQUIRE(result==Status::Win);
}
TEST_CASE( "play 2" ) {
    Game game;
    game.play(1);
    REQUIRE(game.status()==Status::Running);
    game.play(2);
    REQUIRE(game.status()==Status::Running);
    game.play(3);
    REQUIRE(game.status()==Status::Running);
    game.play(4);
    REQUIRE(game.status()==Status::Running);
    game.play(5);
    REQUIRE(game.status()==Status::Lost);
}

TEST_CASE( "runGame 1" ) {
    Game game;
    game.newGame(29);
    game.ostringstream oss;
    std::istringstream iss("12\n29\n");
    runGame(game,oss,iss);
    REQUIRE(oss.str()=="number?\ntoo low\nnumber?\nfound\n");
}

