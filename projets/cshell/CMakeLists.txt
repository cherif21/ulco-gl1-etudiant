cmake_minimum_required( VERSION 3.0 )
project( cshell )
include_directories( include )

# library
add_library( cshell-lib
    src/cshell/cshell.cpp )

# executable
add_executable( cshell-app
  app/main.cpp )
target_link_libraries( cshell-app cshell-lib )

# install
install( TARGETS cshell-app DESTINATION bin )

# testing
add_executable( cshell-test
    test/cshell/cshell-test.cpp
    test/main.cpp )
target_link_libraries( cshell-test cshell-lib )
enable_testing()
add_test( NAME cshell-test COMMAND cshell-test )

