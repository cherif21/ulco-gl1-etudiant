#pragma once

#include <iostream>
enum class Result{TooHigh,TooLow,Found,Invalid};
enum class Status{Running,Win,Lost};
///\brief Classe du jeu Gess
///
class Game{
    private:
        int _target;
        int _nbTries;
        bool _found;
    public:
        ///\brief Nouveau jeu avec un nombre a trouver
        ///
        void newGame(int target);
        ///\brief Nouveau jeu avec un nombrre aleatoire a trouver 
        ///
        void newGame();
        ///\brief Retourne le nombre à trouver
        ///
        int getTarget() const;
         ///\brief jouer un coup
        ///
        Result play(int i);

        Status status() const;

        void RunGame(Game & game, std::ostream & os, std::istream & is);

};

