#include "MyClass.hpp"

#include <catch2/catch.hpp>
#include <sstream>

TEST_CASE( "init & getter", "[MyClass]" ) {
    // TODO créer un objet MyClass et testez le getter mydata
    MyClass myclass;
    std::string result=myclass.mydata();
    std::string excepted="";
    REQUIRE(result==excepted);
}

TEST_CASE( "setter", "[MyClass]" ) {
    // TODO tester le setter mydata
    MyClass myclass;
    myclass.mydata()="foobar";
     std::string result=myclass.mydata();
    std::string excepted="foobar";
    REQUIRE(result==excepted);
}

TEST_CASE( "reset", "[MyClass]" ) {
    // TODO set puis reset puis tester
    MyClass myclass;
    myclass.mydata()="foobar";
    myclass.reset();
    std::string result=myclass.mydata();
    std::string excepted="";
    REQUIRE(result==excepted);

}

TEST_CASE( "fail", "[MyClass]" ) {
    // TODO tester que fail lance une exception (et tester le contenu de cette exception)
    MyClass myclass;
    try{
        myclass.fail();
        REQUIRE(false);
    }catch(const std::string & e){
        REQUIRE(e=="this is MyClass::fail");
    }catch(...){
        REQUIRE(false);
    }
    
}

TEST_CASE( "sqrt2", "[MyClass]" ) {
    // TODO tester le résultat de sqrt2 à 0.001 près
    MyClass myclass;
    double r1=myclass.sqrt2();
    double r2=1.414;
    double d=fabs(r1-r2);
    REQUIRE(d<=0.001);
    
}

TEST_CASE( "operator<<", "[MyClass]" ) {
    // TODO tester l'operateur << (après un set).
    // Indication : utiliser std::ostringstream.
    
    MyClass myclass;
    myclass.mydata()="foobar";
    std::ostringstream oss;
    oss<<myclass;
    std::string result=oss.str();
    std::string excepted="foobar";
    REQUIRE(result==excepted);
}

